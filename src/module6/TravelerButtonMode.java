package module6;

import processing.core.PApplet;

/**
 * Button for chose Traveler mode or not.
 * Traveler mode - you could choose the countries where you have been (visited).
 * This button I will display in the key filed of my applet.
 */
class TravelerButtonMode {

    private int x;
    private int y;
    private int width;
    private int height;
    private PApplet applet;

    /**
     * Just a constructor to button
     * @param applet applet where button will be placed
     * @param x x position
     * @param y y position
     * @param width width of button
     * @param height height of button
     */
    public TravelerButtonMode(PApplet applet, int x, int y, int width, int height) {
        this.applet = applet;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Just a draw method for drawing on applet
     * @param isOnMode
     */
    public void draw(boolean isOnMode) {
        if (isOnMode) {
            // If traveler mode was selected then color in green and text "ON"
            applet.fill(102, 204, 0);
            applet.rect(x, y, width, height);
            applet.fill(0);
            applet.text("Traveler ON", x + 5, y + 10);
        } else {
            // If traveler mode was selected then color in grey and text "OFF"
            applet.fill(160, 160, 160);
            applet.rect(x, y, width, height);
            applet.fill(0);
            applet.text("Traveler OFF", x + 5, y + 10);
        }
    }

    /**
     * Check does the button was selected by user.
     * Checking the mouse position.
     * @return true - button was selected
     *         false - button was not selected
     */
    public boolean isSelected() {
        return applet.mouseX >= this.x && applet.mouseY >= this.y && applet.mouseX <= this.x + this.width && applet.mouseY <= this.y + this.height;
    }
}
