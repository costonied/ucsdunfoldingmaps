package module4;

import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import processing.core.PGraphics;

/** Implements a visual marker for cities on an earthquake map
 * 
 * @author UC San Diego Intermediate Software Development MOOC team
 * @author Your name here
 *
 */
public class CityMarker extends SimplePointMarker {
	
	// The size of the triangle marker
	public static final int TRI_SIZE = 5;
	public static final String LEGEND_TEXT = "City Marker";
	
	public CityMarker(Location location) {
		super(location);
	}
	
	
	public CityMarker(Feature city) {
		super(((PointFeature)city).getLocation(), city.getProperties());
	}
	
	/**
	 * Implementation of method to draw marker on the map.
	 */
	public void draw(PGraphics pg, float x, float y) {
		fillAndShape(pg, x, y);
	}
	
	/* Local getters for some city properties.  You might not need these 
	 * in module 4. 	 */
	public String getCity()
	{
		return getStringProperty("name");
	}
	
	public String getCountry()
	{
		return getStringProperty("country");
	}
	
	public float getPopulation()
	{
		return Float.parseFloat(getStringProperty("population"));
	}

	public static void fillAndShape(PGraphics pg, float x, float y) {
		// Save previous drawing style
		pg.pushStyle();

		pg.fill(153, 0, 0);
		pg.triangle(x, y-TRI_SIZE, x+TRI_SIZE, y, x-TRI_SIZE, y);

		// Restore previous drawing style
		pg.popStyle();
	}
	
}
