package module4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.GeoJSONReader;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.AbstractShapeMarker;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.marker.MultiMarker;
import de.fhpotsdam.unfolding.providers.Google;
import de.fhpotsdam.unfolding.providers.OpenStreetMap;
import de.fhpotsdam.unfolding.providers.MBTilesMapProvider;
import de.fhpotsdam.unfolding.utils.MapUtils;
import parsing.ParseFeed;
import processing.core.PApplet;

/** EarthquakeCityMap
 * An application with an interactive map displaying earthquake data.
 * Author: UC San Diego Intermediate Software Development MOOC team
 * @author Your name here
 * Date: July 17, 2015
 * */
public class EarthquakeCityMap extends PApplet {
	
	// We will use member variables, instead of local variables, to store the data
	// that the setUp and draw methods will need to access (as well as other methods)
	// You will use many of these variables, but the only one you should need to add
	// code to modify is countryQuakes, where you will store the number of earthquakes
	// per country.
	
	// You can ignore this.  It's to get rid of eclipse warnings
	private static final long serialVersionUID = 1L;

	// IF YOU ARE WORKING OFFILINE, change the value of this variable to true
	private static final boolean offline = false;
	
	/** This is where to find the local tiles, for working without an Internet connection */
	public static String mbTilesString = "blankLight-1-3.mbtiles";
	
	

	//feed with magnitude 2.5+ Earthquakes
	private String earthquakesURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.atom";
	
	// The files containing city names and info and country names and info
	private String cityFile = "city-data.json";
	private String countryFile = "countries.geo.json";
	
	// The map
	private UnfoldingMap map;
	
	// Markers for each city
	private List<Marker> cityMarkers;
	// Markers for each earthquake
	private List<Marker> quakeMarkers;

	// A List of country markers
	private List<Marker> countryMarkers;
	
	public void setup() {		
		// (1) Initializing canvas and map tiles
		size(900, 700, OPENGL);
		if (offline) {
		    map = new UnfoldingMap(this, 200, 50, 650, 600, new MBTilesMapProvider(mbTilesString));
		    earthquakesURL = "2.5_week.atom";  // The same feed, but saved August 7, 2015
		}
		else {
//			map = new UnfoldingMap(this, 200, 50, 650, 600, new Google.GoogleMapProvider());
			map = new UnfoldingMap(this, 200, 50, 650, 600, new OpenStreetMap.OpenStreetMapProvider());
			// IF YOU WANT TO TEST WITH A LOCAL FILE, uncomment the next line
		    //earthquakesURL = "2.5_week.atom";
		}
		MapUtils.createDefaultEventDispatcher(this, map);
		
		// FOR TESTING: Set earthquakesURL to be one of the testing files by uncommenting
		// one of the lines below.  This will work whether you are online or offline
		//earthquakesURL = "test1.atom";
		//earthquakesURL = "test2.atom";
		
		// WHEN TAKING THIS QUIZ: Uncomment the next line
		earthquakesURL = "quiz1.atom";
		
		
		// (2) Reading in earthquake data and geometric properties
	    //     STEP 1: load country features and markers
		List<Feature> countries = GeoJSONReader.loadData(this, countryFile);
		countryMarkers = MapUtils.createSimpleMarkers(countries);
		
		//     STEP 2: read in city data
		List<Feature> cities = GeoJSONReader.loadData(this, cityFile);
		cityMarkers = new ArrayList<Marker>();
		for(Feature city : cities) {
		  cityMarkers.add(new CityMarker(city));
		}
	    
		//     STEP 3: read in earthquake RSS feed
	    List<PointFeature> earthquakes = ParseFeed.parseEarthquake(this, earthquakesURL);
	    quakeMarkers = new ArrayList<Marker>();
	    
	    for(PointFeature feature : earthquakes) {
		  //check if LandQuake
		  if(isLand(feature)) {
		    quakeMarkers.add(new LandQuakeMarker(feature));
		  }
		  // OceanQuakes
		  else {
		    quakeMarkers.add(new OceanQuakeMarker(feature));
		  }
	    }

	    // could be used for debugging
	    printQuakes();
	 		
	    // (3) Add markers to map
	    //     NOTE: Country markers are not added to the map.  They are used
	    //           for their geometric properties
	    map.addMarkers(quakeMarkers);
	    map.addMarkers(cityMarkers);
	    
	}  // End setup

	public void draw() {
		background(230);
		map.draw();
		addKey();
		
	}
	
	// helper method to draw key in GUI
	private void addKey() {

		float xText = 75.0f;
		float xShape = 50.0f;

		// Remember you can use Processing's graphics methods here
		fill(255, 250, 240);
		rect(25, 50, 150, 250);
		
		fill(0);
		textAlign(LEFT, CENTER);
		textSize(12);
		text("Earthquake Key", 50, 75);

		noFill();
		CityMarker.fillAndShape(this.g, xShape, 120);
		LandQuakeMarker.drawKeyLegend(this.g, xShape, 140, CityMarker.TRI_SIZE);
		OceanQuakeMarker.drawKeyLegend(this.g, xShape, 160, CityMarker.TRI_SIZE);

		fill(0, 0, 0);
		text(CityMarker.LEGEND_TEXT, xText, 120);
		text(LandQuakeMarker.LEGEND_TEXT, xText, 140);
		text(OceanQuakeMarker.LEGEND_TEXT, xText, 160);
		text("Size ~ Magnitude", xShape - CityMarker.TRI_SIZE, 180);

		/* Color od depth */

		fill(255, 255, 102);
		LandQuakeMarker.drawKeyLegend(this.g, xShape, 210, CityMarker.TRI_SIZE);
		fill(0, 153, 255);
		LandQuakeMarker.drawKeyLegend(this.g, xShape, 230, CityMarker.TRI_SIZE);
		fill(255, 0, 0);
		LandQuakeMarker.drawKeyLegend(this.g, xShape, 250, CityMarker.TRI_SIZE);
		noFill();
		LandQuakeMarker.drawKeyLegend(this.g, xShape, 270, CityMarker.TRI_SIZE);

		fill(0, 0, 0);
		text("Shallow", xText, 210);
		text("Intermediate", xText, 230);
		text("Deep", xText, 250);
		text("Past day", xText, 270);
		textSize(CityMarker.TRI_SIZE*4);
		text("X", xShape - CityMarker.TRI_SIZE - 1, 270-2);
	}


	// Checks whether this quake occurred on land.  If it did, it sets the 
	// "country" property of its PointFeature to the country where it occurred
	// and returns true.  Notice that the helper method isInCountry will
	// set this "country" property already.  Otherwise it returns false.
	private boolean isLand(PointFeature earthquake) {

		// Loop over all the country markers.  
		// For each, check if the earthquake PointFeature is in the 
		// country in m.  Notice that isInCountry takes a PointFeature
		// and a Marker as input.  
		// If isInCountry ever returns true, isLand should return true.
		for (Marker country : countryMarkers) {
			if (isInCountry(earthquake, country))
				return true;
		}

		// not inside any country
		return false;
	}
	
	/* prints countries with number of earthquakes as
	 * Country1: numQuakes1
	 * Country2: numQuakes2
	 * ...
	 * OCEAN QUAKES: numOceanQuakes
	 * */
	private void printQuakes() 	{

		List<Marker> oceanEarthquakes = new ArrayList<>();
		Map<String, Integer> countryEarthquakes;

		// Init map with countries and counts of earthquakes
		countryEarthquakes = countryMarkers.stream()
				.collect(Collectors.toMap(country -> (String) country.getProperty("name"), country -> 0));

		// Считаем количество землетрясений для каждой из стран,
		// записывая результат в результирующий словарь countryEarthquakes.
		// Если землетрясение было не на земле, то добавляем маркер в список oceanEarthquakes
		quakeMarkers.stream()
				// Фильтруем маркеры по типу EarthquakeMarker
				.filter(quakeMarker -> quakeMarker instanceof EarthquakeMarker)
				.forEach(quakeMarker -> {
					// Каждое подходящее землетрсяение приписываем нужной стране
					if (((EarthquakeMarker) quakeMarker).isOnLand) {
						String countryName = (String) quakeMarker.getProperty("country");
						int currentCount = countryEarthquakes.getOrDefault(countryName, 0);
						countryEarthquakes.put(countryName, currentCount + 1);
					} else {
						// Если не на земле, то в список океанских
						oceanEarthquakes.add(quakeMarker);
					}
				});

		countryEarthquakes.forEach((key, value) -> {
			if (value > 0) System.out.println(key + ": " + value);
		});
		System.out.println("OCEAN QUAKES: " + oceanEarthquakes.size());
	}

	// helper method to test whether a given earthquake is in a given country
	// This will also add the country property to the properties of the earthquake 
	// feature if it's in one of the countries.
	// You should not have to modify this code
	private boolean isInCountry(PointFeature earthquake, Marker country) {
		// getting location of feature
		Location checkLoc = earthquake.getLocation();

		// some countries represented it as MultiMarker
		// looping over SimplePolygonMarkers which make them up to use isInsideByLoc
		if(country.getClass() == MultiMarker.class) {
				
			// looping over markers making up MultiMarker
			for(Marker marker : ((MultiMarker)country).getMarkers()) {
					
				// checking if inside
				if(((AbstractShapeMarker)marker).isInsideByLocation(checkLoc)) {
					earthquake.addProperty("country", country.getProperty("name"));
						
					// return if is inside one
					return true;
				}
			}
		}
			
		// check if inside country represented by SimplePolygonMarker
		else if(((AbstractShapeMarker)country).isInsideByLocation(checkLoc)) {
			earthquake.addProperty("country", country.getProperty("name"));
			
			return true;
		}
		return false;
	}

}
