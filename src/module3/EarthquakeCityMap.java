package module3;

//Java utilities libraries
import java.util.List;
import java.util.ArrayList;

//Processing library
import processing.core.PShape;
import processing.core.PApplet;

//Unfolding libraries
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.utils.MapUtils;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.providers.OpenStreetMap;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import de.fhpotsdam.unfolding.providers.MBTilesMapProvider;

//Parsing library
import parsing.ParseFeed;

/** EarthquakeCityMap
 * An application with an interactive map displaying earthquake data.
 * Author: UC San Diego Intermediate Software Development MOOC team
 *
 * @author SavinI
 * Date: March 10, 2020
 * */
public class EarthquakeCityMap extends PApplet {

	// You can ignore this. It's to keep eclipse from generating a warning.
	private static final long serialVersionUID = 1L;

	// IF YOU ARE WORKING OFFLINE, change the value of this variable to true
	private static final boolean offline = false;
	
	// Less than this threshold is a light earthquake
	public static final float THRESHOLD_MODERATE = 5;
	// Less than this threshold is a minor earthquake
	public static final float THRESHOLD_LIGHT = 4;

	// Radius of markers
	private static final float SMALL_SIZE = 6.0f;
	private static final float MIDDLE_SIZE = 9.0f;
	private static final float LARGE_SIZE = 12.0f;

	/** This is where to find the local tiles, for working without an Internet connection */
	public static String mbTilesString = "blankLight-1-3.mbtiles";
	
	// The map
	private UnfoldingMap map;
	
	//feed with magnitude 2.5+ Earthquakes
	private String earthquakesURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.atom";

	// Here is an example of how to use Processing's color method to generate
	// an int that represents the color yellow.
	int yellow = color(255, 255, 0);
	int gray = color(160, 160, 160);
	int red = color(255, 0, 0);
	int white = color(255, 255, 255);
	int black = color(0, 0, 0);

	// Shapes for map legend
	PShape circleRed;
	PShape circleGray;
	PShape circleYellow;
	
	public void setup() {

		size(950, 600, OPENGL);
		this.background(white);

		if (offline) {
		    map = new UnfoldingMap(this, 200, 50, 700, 500, new MBTilesMapProvider(mbTilesString));
		    earthquakesURL = "2.5_week.atom"; 	// Same feed, saved Aug 7, 2015, for working offline
		}
		else {
//			map = new UnfoldingMap(this, 200, 50, 700, 500, new Google.GoogleMapProvider());
			map = new UnfoldingMap(this, 200, 50, 700, 500, new OpenStreetMap.OpenStreetMapProvider());

			// IF YOU WANT TO TEST WITH A LOCAL FILE, uncomment the next line
			//earthquakesURL = "2.5_week.atom";
		}
		
	    map.zoomToLevel(2);
	    MapUtils.createDefaultEventDispatcher(this, map);	
			
	    // The List you will populate with new SimplePointMarkers
	    List<Marker> markers = new ArrayList<Marker>();

	    //Use provided parser to collect properties for each earthquake
	    //PointFeatures have a getLocation method
	    List<PointFeature> earthquakes = ParseFeed.parseEarthquake(this, earthquakesURL);
	    
	    // Step 3: A loop that calls createMarker to create a new SimplePointMarker
		// for each PointFeature in earthquakes. Then each new SimplePointMarker
		// added to the List markers (so that it will be added to the map in the line below)
		for (PointFeature pointFeature : earthquakes) {
			markers.add(createMarker(pointFeature));
		}
	    
	    // Add the markers to the map so that they are displayed
	    map.addMarkers(markers);

		// Init map legend
		createKeys();
	}
		
	/* createMarker: A suggested helper method that takes in an earthquake 
	 * feature and returns a SimplePointMarker for that earthquake
	 * 
	 * In step 3 You can use this method as-is.  Call it from a loop in the 
	 * setup method.
	 * 
	 * Step 4: Adds the proper styling to each marker based on the magnitude of the earthquake
	*/
	private SimplePointMarker createMarker(PointFeature feature)
	{  
		// To print all of the features in a PointFeature (so you can see what they are)
		// uncomment the line below.  Note this will only print if you call createMarker 
		// from setup
		//System.out.println(feature.getProperties());
		
		// Create a new SimplePointMarker at the location given by the PointFeature
		SimplePointMarker marker = new SimplePointMarker(feature.getLocation());
		
		Object magObj = feature.getProperty("magnitude");
		float mag = Float.parseFloat(magObj.toString());
		
		// Step 4: Style the marker's size and color
	    // according to the magnitude of the earthquake
		if (mag < THRESHOLD_LIGHT) {
			marker.setColor(gray);
			marker.setRadius(SMALL_SIZE);
		}
		else if (mag < THRESHOLD_MODERATE) {
			marker.setColor(yellow);
			marker.setRadius(MIDDLE_SIZE);
		}
		else {
			marker.setColor(red);
			marker.setRadius(LARGE_SIZE);
		}
	    
	    // Finally return the marker
	    return marker;
	}
	
	public void draw() {
//		Delete background from here because it not necessary redraw in loop
//	    background(10);
	    map.draw();
	    addKey();
	}


	// helper method to draw key in GUI
	private void addKey() 
	{	
		// Remember you can use Processing's graphics methods here
		shape(circleRed, 20, 50);
		shape(circleGray, 20, 75);
		shape(circleYellow, 20, 100);

		text("Magnitude < 4", 40, 62);
		text("Magnitude < 5", 40, 86);
		text("Magnitude >= 5", 40, 111);
	}

	/**
	 * The function for preparing map legend
	 */
	private void createKeys() {
		float radius = 15.0f;

		circleRed = createShape(ELLIPSE, 0, 0, radius, radius);
		circleGray = createShape(ELLIPSE, 0, 0, radius, radius);
		circleYellow = createShape(ELLIPSE, 0, 0, radius, radius);

		circleRed.setFill(red);
		circleGray.setFill(gray);
		circleYellow.setFill(yellow);

		// Set color white for better text displaying
		fill(black);
	}

}
